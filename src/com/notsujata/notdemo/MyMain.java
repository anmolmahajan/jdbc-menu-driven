package com.notsujata.notdemo;


import java.sql.SQLException;
import java.util.Scanner;

public class MyMain {
	
	public static void displayMenu(Scanner s) throws SQLException, ClassNotFoundException {
		System.out.println("1. Show Employees");
		System.out.println("2. Insert Employee");
		System.out.println("3. Search Employee");
		int choice  = s.nextInt();
		MyDB md = new MyDB();
		switch(choice) {
			case 1:	
					System.out.println(md.dispEmp());
					break;
			case 2:	
					int id = s.nextInt();
					String firstName, lastName, email, jobID;
					String hireDate;
					firstName = s.nextLine();
					lastName = s.nextLine();
					email = s.nextLine();
					jobID = s.nextLine();
					hireDate = s.nextLine();
					System.out.println(md.insertInto(id, firstName, lastName, email, hireDate, jobID));
					break;
			case 3:
					id = s.nextInt();
					System.out.println(md.searchById(id));
					break;
			default:
					s.close();
					return;
		}
		
	}
	
	public static void main(String[] args) throws SQLException, ClassNotFoundException {
		Scanner s = new Scanner(System.in);
		while(true) {
			displayMenu(s);
		}
	}

}
