package com.notsujata.notdemo;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;


public class MyDB {
	static Connection conn = null;
	static PreparedStatement ps = null;
	static Statement st = null;
	static ResultSet res = null;
	
	final String USERNAME = "hr";
	final String PASSWORD = "hr";
	
	
	public MyDB() throws SQLException, ClassNotFoundException {
		Class.forName("oracle.jdbc.driver.OracleDriver");
		conn = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:orcl", USERNAME, PASSWORD);
		st = conn.createStatement();
	}
	
	public String dispEmp() throws SQLException {
		res = st.executeQuery("select * from employees where employee_id < 110");
		String resString = "";
		while(res.next()) {
			resString += "ID: " + res.getInt(1);
			resString += "\tName: " + res.getString(2) + " " + res.getString(3);
		}
		return resString;
	}
	
	public int insertInto(int id, String firstName, String lastName, String email, String hireDate, String jobID) throws SQLException {
		
		String insertTableSQL = "INSERT INTO EMPLOYEES"
				+ "(EMPLOYEE_ID, FIRST_NAME, LAST_NAME, EMAIL, HIRE_DATE, JOB_ID) VALUES"
				+ "(?,?,?,?,?,?)";
		ps = conn.prepareStatement(insertTableSQL);
		ps.setInt(1, id);
		ps.setString(2, firstName);
		ps.setString(3, lastName);
		ps.setString(4, email);
		ps.setString(5, hireDate);
		ps.setString(6, jobID);
		return ps.executeUpdate();
	}
	
	public String searchById(int id) throws SQLException {
		res = st.executeQuery("select * from employees where employee_id=" + id);
		String resString = "";
		while(res.next()) {
			resString += "ID: " + res.getInt(1);
			resString += "\tName: " + res.getString(2) + " " + res.getString(3);
		}
		return resString;
	}
	
}
